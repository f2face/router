<?php

/**
 *	Router.
 *	A tiny routing system library.
 *
 *	by f2face <f2face@f2face.com>
 *	2016
 */

namespace f2face;

class Router
{
	public static $route_list = array();
	
	private static $route_prefix = '';	
	private static $page_404 = null;
	private static $catch_404 = null;
	
    /**
     * Get request URI.
     * 
     * @return string
     */
	private static function getRequestUri()
	{
		$r = !empty($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : $_SERVER['REQUEST_URI'];
		
		if (substr($r, -1, 1) == '/')
			return substr($r, 0, -1);
		
		if (stripos($r, '?') != false) {
			$split = explode('?', $r);
			$r = $split[0];
		}
		
		return $r;
	}
	
    /**
     * Execute application.
     * This method must be called after defining all routes.
     * 
     * @return void
     */
	public static function Execute()
	{
		$request_uri = self::getRequestUri();
		
		foreach (self::$route_list as $route) {
			$match_pattern = '#^/?'.$route['uri'].'$#i';
		
			$preg = preg_match($match_pattern, $request_uri, $match);
			
			if ($preg) {
				unset($match[0]);
				$match = array_values($match);
				
				$arg = $route['arg'];
			
				if (is_callable($arg))
					call_user_func_array($arg, $match);
				elseif (is_array($arg)) {
					if (!empty($arg['name'])) {
						self::$route_list[sizeof(self::$route_list)-1]['name'] = $arg['name'];
					}
					if (!empty($arg['uses'])) {
						list($class, $method) = explode('@', $arg['uses']);				
						call_user_func_array(array($class, $method), $match);
					}
				}
				exit;
			}
		}
		
		if (self::$catch_404 !== null) {
			header('HTTP/1.1 404 Not Found');
			if (is_callable(self::$catch_404))
				call_user_func(self::$catch_404);
			elseif (is_array(self::$catch_404)) {
				if (!empty(self::$catch_404['uses'])) {
					list($class, $method) = explode('@', self::$catch_404['uses']);				
					call_user_func(array($class, $method));
				}
			}
		}
		elseif (self::$page_404 !== null) {
			header('HTTP/1.1 404 Not Found');
			require(self::$page_404);
			exit;
		}
		else {
			header('HTTP/1.1 404 Not Found');
			die('<h1>Not Found</h1>');
		}
	}
	
	/**
     * Catch 404 error, execute some actions or pass to the controller.
     * 
     * @param callable | array $action
     * 
     * @return void
     */
	public static function catch_404($action)
	{
		self::$catch_404 = $action;
	}
	
    /**
     * Set error page for 404.
     * 
     * @param string $file filename of 404 page
     * 
     * @return void
     */
	public static function set_404_page($file)
	{
		self::$page_404 = $file;
	}
	
    /**
     * Define route for GET request.
     * 
     * @param string $uri URI pattern
     * @param callable | array $arg function or array to pass the request to the controller
     * @param string $name route name
     * 
     * @return void
     */
	public static function get($uri, $arg, $name = null)
	{
		if (substr($uri, 0, 1) != '/')
			$uri = '/' . $uri;
		
		if (substr($uri, -1) == '/')
			$uri = substr($uri, 0, -1);
		
		self::$route_list[] = array(
			'uri' => $uri,
			'name' => !empty($name) ? $name : $uri,
			'arg' => $arg
		);
	}
	
    /**
     * Get the route URI.
     * 
     * @param string $route_name name of the route
     * @param array $args argument(s) for route URI pattern
     * @param array $query URL query
     * 
     * @return void
     */
	public static function route($route_name, $args = array(), $query = array())
	{
		$route = array();
		
		foreach (self::$route_list as $r) {
			if ($r['name'] == $route_name) {
				$route = $r;
			}
		}
		
		$to = $route['uri'];
		
		if (!empty($args)) {
			$to = preg_replace_callback('#(\([^\)]+\))#', function($m) use($args){
				static $i = 0;
				$out = $args[$i];
				$i++;
				return $out;
			}, $route['uri']);
		}
		
		if (!empty($query))
			$to .= '?' . http_build_query($query);
		
		$to = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . self::$route_prefix . $to;
		
		return $to;
	}
	
    /**
     * Redirect page to a route.
     * 
     * @param string $route_name 
     * @param array $args  
     * @param array $query  
     * @param int $redirect_code  
     * 
     * @return void
     */
	public static function redirect($route_name, $args = array(), $query = array(), $redirect_code = 302)
	{
		$to = self::route($route_name, $args, $query);
		
		if ($redirect_code == 301)
			header('HTTP/1.1 301 Moved Permanently');
		else
			header('HTTP/1.1 302 Moved Temporarily');
		
		header('Location: ' . $to);
		exit;
	}
	
    /**
     * Set route prefix (global)
     * 
     * @param string $prefix 
     * 
     * @return void
     */
	public static function setPrefix($prefix)
	{
		self::$route_prefix = $prefix;
		
		if (substr($prefix, 0, 1) == '/')
			self::$route_prefix = substr($prefix, 1);
		else
			self::$route_prefix = '/' . $prefix;
		
		if (substr($prefix, -1) == '/')
			self::$route_prefix = substr($prefix, 0, -1);
	}
}